var gulp = require('gulp'), // Gulp JS
    less = require('gulp-less'), // Plugin for less
    csso = require('gulp-csso'), // Minify CSS
    uglify = require('gulp-uglify'), // Minify JS
    concat = require('gulp-concat'), // Concatenation files
    rename = require('gulp-rename'); // Rename files

gulp.task('html', function() {
    gulp.src('./source/*.html')
    .pipe(gulp.dest('./dev/')); // Write
}); 

gulp.task('less', function() {
    gulp.src(['./source/libs/normalize-less/normalize.less', './source/less/*.less'])
    .pipe(less())
    .pipe(concat('common.css')) // Concatenation
    .pipe(gulp.dest('./dev/css'));
});

gulp.task('js', function() {
    gulp.src('./source/js/*.js')
    .pipe(concat('common.js'))
    .pipe(gulp.dest('./dev/js'));
});

gulp.task('watch', function() {
    // Temp build
    gulp.run('html');
    gulp.run('less');
    gulp.run('js');

    gulp.watch('source/*.html', function() {
        gulp.run('html');
    });
    gulp.watch('source/less/*.less', function() {
        gulp.run('less');
    });
    gulp.watch('source/js/*.js', function() {
        gulp.run('js');
    });

    console.log('All right');
});

gulp.task('build', function() {
    // css
    gulp.src(['./source/libs/normalize-less/normalize.less', './source/less/*.less'])
    .pipe(less())
    .pipe(concat('common.css')) // Concatenation
    .pipe(csso()) // minimize css
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest('./dist/css/')) // write css

    // html
    gulp.src('./source/*.html')
    .pipe(gulp.dest('./dist/'))

    // js
    gulp.src(['./node_modules/vanilla-slab/build/vanilla-slab.build.js', './source/js/*.js'])
    .pipe(concat('common.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));

});