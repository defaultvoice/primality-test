;(function(){
	document.onreadystatechange = function() {
		if (document.readyState === 'complete') {
			var mySlab = new VanillaSlab;
			mySlab.init({
				selector: '.slab',
				minWordsPerLine: 2,
				maxWordsPerLine: 2,
				postTweak: true
			});

			var bindElems = document.querySelectorAll('.slab');

			for(var i=0; i<bindElems.length; i++) {
				var elem = bindElems[i];
				binder.call(elem);
				// Chrome don't support DOMAttrModified event :(
				//elem.addEventListener('DOMAttrModified', binder.bind(elem));
				// Пришлось малость заняться манкипатчингом, вызов ф-ии binder перенес в vanilla-slab.build.js
				// в ф-ию setFont
			}


			var primeElem = document.querySelector('.prime');
			var resultElem = document.querySelector('.result');
			primeElem.oninput = function() {
				if(isNaN(this.value)) {
					var val = this.value.replace(/\D/g, "");
					this.value = (isNaN(val) || val == "") ? "" : parseFloat(val);
				}
				if(this.value == '') {
					resultElem.innerHTML = '';
				} else {
					if(isPrime(this.value)) {
						resultElem.innerHTML = 'Да';
					} else {
						resultElem.innerHTML = 'Нет';
					}
				}
			}
		}
	}
}());

function binder() {
	var input = this.nextElementSibling;
	var result = input.nextElementSibling;
	var fontSize = this.firstElementChild.style.fontSize;
	var fontSizeResult = parseFloat(fontSize) + (parseFloat(fontSize)/100)*30 + 'px';
	input.style.fontSize = fontSize;
	result.style.fontSize = fontSizeResult;
	console.log(input.style.fontSize, result.style.fontSize)
};

// AKS Primality test

function expand_x_1(p) {
	var ex = [1];
	for(var i = 0; i < p; i++) {
		ex.push(ex[ex.length - 1] * -(p-i) / (i + 1))
	}
	return ex.reverse()
}

function isPrime(p) {
	if (isNaN(p)) {
		throw new TypeError('Not a number!')
	}
	var p = parseFloat(p);
	if (p < 2) return false;
	var ex = expand_x_1(p);
	ex[0] += 1;
	for(var i = 0; i<ex.length-1; i++) {
		var mult = ex[i];
		if(mult % p) {
			return false
		}
	}
	return true
}

