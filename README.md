# README #

### What is this repository for? ###

* Examination for GRISSLI Ltd.
* v.0.0.0

### How do I get set up? ###

**Install Requirements:**

* nodejs
* bower
* gulp

**Build the project:**

* git clone https://defaultvoice@bitbucket.org/defaultvoice/primality-test.git
* cd primality-test
* npm install
* bower install
* gulp build


### Who do I talk to? ###

* Mikhail Frolkov <mikhail_frolkov@outlook.com>